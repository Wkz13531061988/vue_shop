import Vue from 'vue'
import {Tag,alert,steps,step,tabs,tabPane,Cascader,Option,MessageBox,Dialog,Pagination,Tooltip,Switch,TableColumn,Select,Tree,
    Table,Row,Col,Card,BreadcrumbItem,Breadcrumb,Button, Form, FormItem, 
    Input, Message,Container,Header,Aside,Main,Menu,Submenu,
    MenuItemGroup,MenuItem, } from 'element-ui'
    import Timeline  from './timeline/index'
    import TimelineItem  from './timeline-item/index'
    
Vue.use(Timeline)
Vue.use(TimelineItem)
Vue.use(tabs)
Vue.use(tabPane)
Vue.use(alert)
Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(MenuItem)
Vue.use(MenuItemGroup)
Vue.use(Submenu)
Vue.use(Menu)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Card)
Vue.use(Row)
Vue.use(Col)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Switch)
Vue.use(Tooltip)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Tag)
Vue.use(Tree)
Vue.use(Select)
Vue.use(Option)
Vue.use(Cascader)
Vue.use(steps)
Vue.use(step)


Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
