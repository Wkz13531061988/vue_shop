import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import './assets/css/global.css'
import axios from 'axios'
import TreeTable from 'vue-table-with-tree-grid'


axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'

axios.interceptors.request.use(config=>{
  config.headers.Authorization = sessionStorage.getItem('token')
  return config
})
Vue.filter('dateFormat',function(origin){
  let dt=new Date(origin)
   
  let y=dt.getFullYear()
  let m=(dt.getMonth()+1+'').padStart(2,0)
  let d=(dt.getDate()+'').padStart(2,0)

  let hh=(dt.getHours()+'').padStart(2,0)
  let mm=(dt.getMinutes()+'').padStart(2,0)
  let ss=(dt.getSeconds()+'').padStart(2,0)
 
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`

})
Vue.prototype.$http=axios
Vue.config.productionTip = false
Vue.component('tree-table',TreeTable)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
